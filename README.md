## Backend to Frontend Integration Exercise

#### Task Description:

Create a simple UI that will display the data from the Onboarding Exercise.
Your output should be able to perform the CRUD functionality and changes should
reflect on the data inside your table.

#### Setup

1. Clone the repository to your `xampp/htdocs` directory.
2. Navigate to the `client` folder and install the Node packages using this command: `npm install`.
3. Navigate to the `server` folder and install the necessary packages using Composer with this command: `composer install`.
4. Create a database named 'onboarding_task_2_db'.
5. import the backup database into MySQL. You can find the backup file in the `database` folder.

#### Start The Application

to run the client server, navigate to the `client` folder and type the following command:

    npm run dev
